package model;

import java.util.ArrayList;

public interface ICluster {
	
	ArrayList<IDomain> getDomainsList();
	
	void setDomainsList(ArrayList<IDomain> domainsList);
}
