package model;

import java.util.ArrayList;

public interface IStudentEvidence {
	
	ArrayList<IStudentPlan> getStudentPlansList();
	
	void setStudentPlansList(ArrayList<IStudentPlan> studentPlans);
}
