package model;

import java.io.Serializable;
import java.util.ArrayList;



public class Cluster implements ICluster, Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<IDomain> domainsList;
	
	public Cluster() {
		domainsList = new ArrayList<IDomain>();
	}

	@Override
	public ArrayList<IDomain> getDomainsList() {
		// TODO Auto-generated method stub
		return domainsList;
	}

	@Override
	public void setDomainsList(ArrayList<IDomain> domainsList) {
		// TODO Auto-generated method stub
		this.domainsList = domainsList;
	}
	
	
}
