package model;

import java.util.ArrayList;

public interface ICareer {

	String getCareerName();
	
	String getCareerDescription();
	
	ArrayList<ISubject> getFirstYearSubjects();
	
	ArrayList<ISubject> getSecondYearSubjects();
	
	ArrayList<ISubject> getThirdYearSubjects();
	
	boolean checkIfThereAreEnoughSubjects(ArrayList<ISubject> subjectsList);
	
	void addSubjects(ISubject subject, int studyYear);
}
