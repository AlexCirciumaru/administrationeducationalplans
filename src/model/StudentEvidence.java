package model;

import java.io.Serializable;
import java.util.ArrayList;

public class StudentEvidence implements IStudentEvidence, Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<IStudentPlan> studentPlans;
	
	public StudentEvidence() {
		// TODO Auto-generated constructor stub
		studentPlans = new ArrayList<>();
	}
	
	@Override
	public ArrayList<IStudentPlan> getStudentPlansList() {
		// TODO Auto-generated method stub
		return studentPlans;
	}

	@Override
	public void setStudentPlansList(ArrayList<IStudentPlan> studentPlans) {
		// TODO Auto-generated method stub
		this.studentPlans = studentPlans;
	}
	
	
}
