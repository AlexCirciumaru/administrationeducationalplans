package model;

public interface IStudentPlan {
	
	String getStudentLastName();
	
	String getStudentFirstName();
	
	String getCareerName();
}
