package model;

import java.io.Serializable;
import java.util.ArrayList;

public class Career implements ICareer, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String careerName;
	private String careerDescription;
	private ArrayList<ISubject> firstYearSubjects;
	private ArrayList<ISubject> secondYearSubjects;
	private ArrayList<ISubject> thirdYearSubjects;
	
	public Career(String careerName, String careerDescription, ArrayList<ISubject> firstYearSubjects,
			ArrayList<ISubject> secondYearSubjects, ArrayList<ISubject> thirdYearSubjects) {
		super();
		this.careerName = careerName;
		this.careerDescription = careerDescription;
		this.firstYearSubjects = firstYearSubjects;
		this.secondYearSubjects = secondYearSubjects;
		this.thirdYearSubjects = thirdYearSubjects;
	}

	@Override
	public String getCareerName() {
		// TODO Auto-generated method stub
		return careerName;
	}

	@Override
	public String getCareerDescription() {
		// TODO Auto-generated method stub
		return careerDescription;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return getCareerName();
	}

	@Override
	public ArrayList<ISubject> getFirstYearSubjects() {
		// TODO Auto-generated method stub
		return firstYearSubjects;
	}

	@Override
	public ArrayList<ISubject> getSecondYearSubjects() {
		// TODO Auto-generated method stub
		return secondYearSubjects;
	}

	@Override
	public ArrayList<ISubject> getThirdYearSubjects() {
		// TODO Auto-generated method stub
		return thirdYearSubjects;
	}

	@Override
	public boolean checkIfThereAreEnoughSubjects(ArrayList<ISubject> subjectsList) {
		// TODO Auto-generated method stub
		if(subjectsList.size() < 3) {
			return false;
		}
		return true;
	}

	@Override
	public void addSubjects(ISubject subject, int studyYear) {
		// TODO Auto-generated method stub
		
		switch (studyYear) {
		case 1:
			firstYearSubjects.add(subject);
			break;
		case 2:
			secondYearSubjects.add(subject);
			break;
		case 3:
			thirdYearSubjects.add(subject);
			break;
		default:
			break;
		}
		
		System.out.println(firstYearSubjects + " + " + secondYearSubjects + " + " + thirdYearSubjects);
	}


}
