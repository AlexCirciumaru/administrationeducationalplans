package model;

import java.io.Serializable;

public class StudentPlan implements IStudentPlan, Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private IStudent student;
	private String careerName;
	
	public StudentPlan(IStudent student, String careerName) {
		// TODO Auto-generated constructor stub
		this.student = student;
		this.careerName = careerName;
	}
	
	@Override
	public String getStudentLastName() {
		// TODO Auto-generated method stub
		return student.getStudentLastName();
	}

	@Override
	public String getStudentFirstName() {
		// TODO Auto-generated method stub
		return student.getStudentFirstName();
	}

	@Override
	public String getCareerName() {
		// TODO Auto-generated method stub
		return careerName;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return student.getStudentLastName() + " + " + student.getStudentFirstName() + " + " + careerName;
	}
	
	
}
