package model;

import java.io.Serializable;
import java.util.ArrayList;

public class Domain implements IDomain, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String domainName;
	private ArrayList<ICareer> careersList;

	public Domain(String domainName, ArrayList<ICareer> careersList) {
		super();
		this.domainName = domainName;
		this.careersList = careersList;
	}

	@Override
	public String getDomainName() {
		// TODO Auto-generated method stub
		return domainName;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return getDomainName();
	}

	@Override
	public ArrayList<ICareer> getCareerList() {
		// TODO Auto-generated method stub
		return careersList;
	}

	@Override
	public void setDomainName(String domainName) {
		// TODO Auto-generated method stub
		this.domainName = domainName;
	}

	@Override
	public void setCareersList(ArrayList<ICareer> careersList) {
		// TODO Auto-generated method stub
		this.careersList = careersList;
	}
}
