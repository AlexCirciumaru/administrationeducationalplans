package model;

public interface IStudent {
	
	String getStudentFirstName();
	String getStudentLastName();
}
