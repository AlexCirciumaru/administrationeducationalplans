package model;

import java.util.ArrayList;

public interface IDomain {

	String getDomainName();
	
	ArrayList<ICareer> getCareerList();
	
	void setDomainName(String name);
	
	void setCareersList(ArrayList<ICareer> careersList);
}
