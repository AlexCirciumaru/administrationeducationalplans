package model;

import java.io.Serializable;

public class Student implements IStudent, Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String studentFirstName;
	private String studentLastName;
	
	@Override
	public String getStudentFirstName() {
		// TODO Auto-generated method stub
		return studentFirstName;
	}
	@Override
	public String getStudentLastName() {
		// TODO Auto-generated method stub
		return studentLastName;
	}
	public Student(String studentFirstName, String studentLastName) {
		super();
		this.studentFirstName = studentFirstName;
		this.studentLastName = studentLastName;
	}
}
