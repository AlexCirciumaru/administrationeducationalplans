package model;

import java.io.Serializable;

public class Subject implements ISubject, Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String subjectName;
	private int yearStudy;

	public Subject(String subjectName, int yearStudy) {
		super();
		this.subjectName = subjectName;
		this.yearStudy = yearStudy;
	}

	@Override
	public String getSubjectName() {
		// TODO Auto-generated method stub
		return subjectName;
	}

	@Override
	public int getYearStudy() {
		// TODO Auto-generated method stub
		return yearStudy;
	}

	@Override
	public void update(String subjectName, int yearStudy) {
		// TODO Auto-generated method stub
		this.subjectName = subjectName;
		this.yearStudy = yearStudy;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return getSubjectName();
	}
	
	
}
