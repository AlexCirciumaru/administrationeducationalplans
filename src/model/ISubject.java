package model;

public interface ISubject {
	
	String getSubjectName();
	
	int getYearStudy();
	
	void update(String subjectName, int yearStudy);
}
