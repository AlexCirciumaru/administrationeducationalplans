package controllers;

import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JOptionPane;

import factories.CareerFactory;
import factories.DomainFactory;
import factories.SubjectFactory;
import model.Career;
import model.Cluster;
import model.ICareer;
import model.ICluster;
import model.IDomain;
import model.ISubject;
import serialize.DomainsSerializer;
import serialize.IDomainsSerializer;

public class AddAnEducationPlanController {
	
	private ICluster cluster;
	private IDomain domain;
	private ICareer career;
	private ISubject subject;
	private SubjectFactory subjectFactory;
	private DomainFactory domainFactory;
	private CareerFactory careeerFactory;
	private IDomainsSerializer serializer;
	
	public AddAnEducationPlanController() {
		super();
		serializer = new DomainsSerializer();
		career = new Career(new String(), new String(), new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
		subjectFactory = new SubjectFactory();
		domainFactory = new DomainFactory();
		careeerFactory = new CareerFactory();
		createCluster();
	}
	
	public void addSubject(String subjectName, int studyYear) {
		
		subject = subjectFactory.createSubject(subjectName, studyYear); 
		
		career.addSubjects(subject, studyYear);
	}
	
	public void createCluster() {
		
		try {
			
			cluster = serializer.deserialize("domainsList.ser");
			
		} catch (Exception e) {
			// TODO: handle exception
			cluster = new Cluster();
		}
	}
	
	public void addDomain(String text) {
		
		domain = domainFactory.createDomain(text, new ArrayList<>());
		cluster.getDomainsList().add(domain);
		System.out.println(cluster.getDomainsList());	
	}
	
	public void createEducationPlan(IDomain domain, String careerName, String careerDescription) {
		
		career = careeerFactory.createCareer(careerName, careerDescription, career.getFirstYearSubjects(), career.getSecondYearSubjects(), career.getThirdYearSubjects());
		domain.getCareerList().add(career);
		ArrayList<IDomain> temporaryDomainList = cluster.getDomainsList();
		int index = 0;
		
		for(IDomain domainIterator : cluster.getDomainsList()) {
			
			if(domainIterator.getDomainName().equals(domain.getDomainName())) {
				
				cluster.getDomainsList().set(index, domain);
			}
			index = index + 1;
		}
		
		serializer.serialize(cluster, "domainsList.ser");
		System.out.println(domain.toString() + " " + domain.getCareerList());
		System.out.println("First year subjects : " + career.getFirstYearSubjects() + " , Second Year Subjects : " + career.getSecondYearSubjects() + " , Third Year Subjects : " + career.getThirdYearSubjects());
		career = new Career(new String(), new String(), new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
	}
	
	public boolean checkIfListsAreEmpty() {
		
		if(career.getFirstYearSubjects().isEmpty()) {
			
			JOptionPane.showMessageDialog(null, "Please add a first year subject", "input error", 0);
			return true;
		} else if (career.getSecondYearSubjects().isEmpty()) {
			
			JOptionPane.showMessageDialog(null, "Please add a second year subject", "input error", 0);
			return true;
		} else if (career.getThirdYearSubjects().isEmpty()) {
			
			JOptionPane.showMessageDialog(null, "Please add a third year subject.", "input error", 0);
			return true;
		}
		
		return false;
	}
}
