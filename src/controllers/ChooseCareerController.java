package controllers;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JTextArea;

import factories.StudentFactory;
import factories.StudentPlanFactory;
import gui.UserMenu;
import model.Career;
import model.ICareer;
import model.IDomain;
import model.IStudent;
import model.IStudentEvidence;
import model.IStudentPlan;
import model.StudentEvidence;
import serialize.IStudentPlanSerializer;
import serialize.StudentPlanSerializer;
import verifier.CheckInputs;

public class ChooseCareerController implements IController{
	
	private IStudent student;
	private IStudentPlan studentPlan;
	private IStudentEvidence studentEvidence;
	private StudentFactory studentFactory;
	private IStudentPlanSerializer studentSerializer;
	private StudentPlanFactory studentPlanFactory;
	private CheckInputs checkInputs;
	
	public ChooseCareerController() {
		// TODO Auto-generated constructor stub
		studentSerializer = new StudentPlanSerializer();
		studentFactory = new StudentFactory();
		studentPlanFactory = new StudentPlanFactory();
		//studentEvidence = new StudentEvidence();
		studentEvidence = studentSerializer.deserialize("studentPlansList.ser");
		checkInputs = new CheckInputs();
	}
	
	@Override
	public void btnBackIsPressed(JFrame frame) {
		// TODO Auto-generated method stub
		
		UserMenu mainMenu = new UserMenu();
		mainMenu.getFrame().setVisible(true);
		frame.dispose();
	}
	
	public void updateTextFieldDescription(JTextArea textArea, JComboBox<IDomain> comboBox) {
		
		ICareer career = (Career) comboBox.getSelectedItem();
		textArea.setText(career.getCareerDescription());
	}
	
	public void createStudentPlan(String studentLastName, String studentFirstName, String careerName) {
		
		student = studentFactory.createStudent(studentFirstName, studentLastName);
		studentPlan = studentPlanFactory.createStudentPlan(student, careerName);
		
		studentEvidence.getStudentPlansList().add(studentPlan);
		studentSerializer.serialize(studentEvidence, "studentPlansList.ser");
		System.out.println(studentPlan.toString());
	}
	
	public void checkStudentDetails() {
		
		
	}
}
