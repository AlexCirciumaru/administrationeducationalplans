package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;

import controllers.UserMenuController;

import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class UserMenu{

	private JFrame frame;
	private UserMenuController controller;
	private JLabel lblEducationPlans;
	private JLabel lblTheWorkingWorld;
	private JLabel lblNeverTooEarly;
	private JButton btnGetStarted;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UserMenu window = new UserMenu();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public UserMenu() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 650, 460);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		controller = new UserMenuController();
		
		lblEducationPlans = new JLabel("BUILD YOUR CAREER");
		lblEducationPlans.setFont(new Font("Sitka Banner", Font.BOLD | Font.ITALIC, 19));
		lblEducationPlans.setBounds(229, 11, 174, 42);
		frame.getContentPane().add(lblEducationPlans);
		
		btnGetStarted = new JButton("GET STARTED");
		btnGetStarted.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnGetStarted.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
				new ChooseCareer().getFrame().setVisible(true);
				frame.dispose();
				
			}
		});
		btnGetStarted.setBounds(141, 219, 370, 120);
		frame.getContentPane().add(btnGetStarted);
		
		lblTheWorkingWorld = new JLabel("The working world may seem like a long way off, but it is");
		lblTheWorkingWorld.setFont(new Font("Segoe UI Emoji", Font.BOLD | Font.ITALIC, 15));
		lblTheWorkingWorld.setBounds(96, 92, 452, 42);
		frame.getContentPane().add(lblTheWorkingWorld);

		lblNeverTooEarly = new JLabel("never too early to plan for your goals.");
		lblNeverTooEarly.setFont(new Font("Segoe UI Emoji", Font.BOLD | Font.ITALIC, 15));
		lblNeverTooEarly.setBounds(172, 135, 308, 27);
		frame.getContentPane().add(lblNeverTooEarly);
	}

	public JFrame getFrame() {
		return frame;
	}
}
