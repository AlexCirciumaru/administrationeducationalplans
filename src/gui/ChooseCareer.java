package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.util.ArrayList;

import javax.swing.JComboBox;
import javax.swing.JTextArea;

import controllers.ChooseCareerController;
import model.Career;
import model.Domain;
import model.ICareer;
import model.ICluster;
import model.IDomain;
import model.ISubject;
import model.Subject;
import serialize.DomainsSerializer;
import verifier.CheckInputs;

import javax.swing.JScrollPane;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JList;
import javax.swing.JOptionPane;

public class ChooseCareer {

	private JFrame frame;
	private ChooseCareerController chooseCareerController;
	private JTextArea textArea;
	private DomainsSerializer domainSerializer;
	private ICluster cluster;
	private IDomain domain;
	private JComboBox careerComboBox;
	private JComboBox domainComboBox;
	private ICareer career;
	private JTextField textFieldLastName;
	private JTextField textFieldFirstName;
	private JScrollPane scrollPane;
	private JScrollPane scrollPaneFirstYear;
	private JList listFirstYear;
	private JScrollPane scrollPaneSecondYear;
	private JList listSecondYear;
	private JScrollPane scrollPaneThirdYear;
	private JList listThirdYear;
	private CheckInputs checkInputs;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ChooseCareer window = new ChooseCareer();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ChooseCareer() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 867, 550);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		chooseCareerController = new ChooseCareerController();
		domainSerializer = new DomainsSerializer();
		checkInputs = new CheckInputs();
		
		cluster = domainSerializer.deserialize("domainsList.ser");
		
		JLabel lblChooseADomain = new JLabel("Choose a domain :");
		lblChooseADomain.setFont(new Font("Sitka Text", Font.BOLD | Font.ITALIC, 15));
		lblChooseADomain.setBounds(45, 21, 145, 33);
		frame.getContentPane().add(lblChooseADomain);
		
		
		JLabel lblChooseACareer = new JLabel("Choose a career :");
		lblChooseACareer.setFont(new Font("Sitka Text", Font.BOLD | Font.ITALIC, 15));
		lblChooseACareer.setBounds(45, 84, 145, 33);
		frame.getContentPane().add(lblChooseACareer);
		
		DefaultComboBoxModel model1 = new DefaultComboBoxModel(cluster.getDomainsList().toArray());
		domainComboBox = new JComboBox<>();
		domainComboBox.setModel(model1);
		domainComboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				domain = (IDomain)domainComboBox.getSelectedItem();
				DefaultComboBoxModel model2 = new DefaultComboBoxModel<>(domain.getCareerList().toArray());
				careerComboBox.setModel(model2);
			}
		});
		domainComboBox.setBounds(209, 26, 184, 22);
		frame.getContentPane().add(domainComboBox);
		
		domain = (IDomain)domainComboBox.getSelectedItem();
		
		DefaultComboBoxModel model2 = new DefaultComboBoxModel<>(domain.getCareerList().toArray());
		careerComboBox = new JComboBox<>();
		careerComboBox.setModel(model2);
		careerComboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				chooseCareerController.updateTextFieldDescription(textArea, careerComboBox);
				career = (ICareer) careerComboBox.getSelectedItem();
			}
		});
		careerComboBox.setBounds(209, 84, 184, 22);
		frame.getContentPane().add(careerComboBox);
		
		JLabel lblCareerDescription = new JLabel("Career Description :");
		lblCareerDescription.setFont(new Font("Sitka Text", Font.BOLD | Font.ITALIC, 15));
		lblCareerDescription.setBounds(45, 162, 145, 33);
		frame.getContentPane().add(lblCareerDescription);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(210, 150, 242, 60);
		frame.getContentPane().add(scrollPane);
		
		textArea = new JTextArea();
		textArea.setEditable(false);
		scrollPane.setViewportView(textArea);
		
		JButton btnNext = new JButton("Finish");
		btnNext.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(!checkInputs.hasOnlyLetters(textFieldLastName.getText())) {
					
					return;
					
				} else if(!checkInputs.hasOnlyLetters(textFieldFirstName.getText())) {
					
					return;
					
				} else {
					
					chooseCareerController.createStudentPlan(textFieldLastName.getText(), textFieldFirstName.getText(), career.getCareerName());
					JOptionPane.showMessageDialog(null, "You created your education plan.", "Congratulations!", 1);
					frame.dispose();
				}
				
			}
		});
		btnNext.setBounds(635, 432, 169, 43);
		frame.getContentPane().add(btnNext);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				chooseCareerController.btnBackIsPressed(frame);
			}
		});
		btnBack.setBounds(45, 432, 169, 43);
		frame.getContentPane().add(btnBack);
		
		JLabel lblLastName = new JLabel("Last Name :");
		lblLastName.setFont(new Font("Sitka Text", Font.BOLD | Font.ITALIC, 15));
		lblLastName.setBounds(45, 246, 133, 43);
		frame.getContentPane().add(lblLastName);
		
		textFieldLastName = new JTextField();
		textFieldLastName.setBounds(209, 251, 230, 33);
		frame.getContentPane().add(textFieldLastName);
		textFieldLastName.setColumns(10);
		
		JLabel lblFirstName = new JLabel("First Name :");
		lblFirstName.setFont(new Font("Sitka Text", Font.BOLD | Font.ITALIC, 15));
		lblFirstName.setBounds(45, 316, 126, 33);
		frame.getContentPane().add(lblFirstName);
		
		textFieldFirstName = new JTextField();
		textFieldFirstName.setBounds(209, 316, 230, 33);
		frame.getContentPane().add(textFieldFirstName);
		textFieldFirstName.setColumns(10);
		
		scrollPaneFirstYear = new JScrollPane();
		scrollPaneFirstYear.setBounds(623, 116, 215, 79);
		frame.getContentPane().add(scrollPaneFirstYear);
		
		listFirstYear = new JList();
		scrollPaneFirstYear.setViewportView(listFirstYear);
		
		scrollPaneSecondYear = new JScrollPane();
		scrollPaneSecondYear.setBounds(623, 218, 215, 79);
		frame.getContentPane().add(scrollPaneSecondYear);
		
		listSecondYear = new JList();
		scrollPaneSecondYear.setViewportView(listSecondYear);
		
		scrollPaneThirdYear = new JScrollPane();
		scrollPaneThirdYear.setBounds(623, 324, 215, 79);
		frame.getContentPane().add(scrollPaneThirdYear);
		
		listThirdYear = new JList();
		scrollPaneThirdYear.setViewportView(listThirdYear);
		
		JButton btnShowSubjects = new JButton("Show subjects");
		btnShowSubjects.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				listFirstYear = new JList<>(career.getFirstYearSubjects().toArray());
				listSecondYear = new JList<>(career.getSecondYearSubjects().toArray());
				listThirdYear = new JList<>(career.getThirdYearSubjects().toArray());
				scrollPaneFirstYear.setViewportView(listFirstYear);
				scrollPaneSecondYear.setViewportView(listSecondYear);
				scrollPaneThirdYear.setViewportView(listThirdYear);
			}
		});
		btnShowSubjects.setBounds(653, 45, 163, 43);
		frame.getContentPane().add(btnShowSubjects);
		
		JLabel lblFirstYear = new JLabel("First Year :");
		lblFirstYear.setFont(new Font("Sitka Text", Font.BOLD | Font.ITALIC, 15));
		lblFirstYear.setBounds(497, 136, 104, 33);
		frame.getContentPane().add(lblFirstYear);
		
		JLabel lblSecondYear = new JLabel("Second Year :");
		lblSecondYear.setFont(new Font("Sitka Text", Font.BOLD | Font.ITALIC, 15));
		lblSecondYear.setBounds(497, 246, 104, 33);
		frame.getContentPane().add(lblSecondYear);
		
		JLabel lblThirdYear = new JLabel("Third Year :");
		lblThirdYear.setFont(new Font("Sitka Text", Font.BOLD | Font.ITALIC, 15));
		lblThirdYear.setBounds(497, 346, 104, 33);
		frame.getContentPane().add(lblThirdYear);
		
	}

	public JFrame getFrame() {
		return frame;
	}
}
