package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import model.IStudentEvidence;
import model.IStudentPlan;
import serialize.StudentPlanSerializer;

import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

public class StudentPlans {

	private JFrame frame;
	private JTable table;
	private JButton btnBack;
	private IStudentEvidence studentEvidence;
	private StudentPlanSerializer serializer;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					//StudentPlans window = new StudentPlans();
					//window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public StudentPlans() {
		initialize();
		populateJTable(table);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 508, 409);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		serializer = new StudentPlanSerializer();
		studentEvidence = serializer.deserialize("studentPlansList.ser");

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(71, 44, 392, 230);
		frame.getContentPane().add(scrollPane);

		table = new JTable();
		scrollPane.setViewportView(table);

		btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				frame.setVisible(false);
			}
		});
		btnBack.setBounds(27, 303, 106, 35);
		frame.getContentPane().add(btnBack);
	}

	public JFrame getFrame() {
		return frame;
	}

	public void populateJTable(JTable table) {

		String tableColumns[] = { "Student's Last Name", "Student's First Name", "Career Name" };
		DefaultTableModel tableModel = new DefaultTableModel(tableColumns, 0);

		for (IStudentPlan studentPlan : studentEvidence.getStudentPlansList()) {
				
			    ArrayList<String> rowData = new ArrayList<String>();
			    rowData.add(studentPlan.getStudentLastName());
			    rowData.add(studentPlan.getStudentFirstName());
			    rowData.add(studentPlan.getCareerName());
				tableModel.addRow(rowData.toArray());

		}

		table.setModel(tableModel);
	}
}
