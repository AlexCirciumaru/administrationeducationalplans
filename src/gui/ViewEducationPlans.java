package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import model.ICareer;
import model.ICluster;
import model.IDomain;
import serialize.DomainsSerializer;

import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

public class ViewEducationPlans {

	private JFrame frame;
	private JTable table;
	private JButton btnBack;
	private AddAnEducationPlan addPlan = new AddAnEducationPlan();
	private ICluster cluster;
	private DomainsSerializer serializer;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					//ViewEducationPlans window = new ViewEducationPlans();
					//window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ViewEducationPlans() {
		initialize();
		populateJTable(table);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 629, 401);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		serializer = new DomainsSerializer();
		cluster = serializer.deserialize("domainsList.ser");

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(92, 47, 372, 215);
		frame.getContentPane().add(scrollPane);

		table = new JTable();
		scrollPane.setViewportView(table);

		btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				frame.setVisible(false);
			}
		});
		btnBack.setBounds(26, 298, 104, 30);
		frame.getContentPane().add(btnBack);

		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				addPlan.getFrame().setVisible(true);
			}
		});
		btnAdd.setBounds(485, 21, 118, 36);
		frame.getContentPane().add(btnAdd);

		JButton btnEdit = new JButton("Edit");
		btnEdit.setBounds(485, 82, 118, 36);
		frame.getContentPane().add(btnEdit);

		JButton btnDelete = new JButton("Delete");
		btnDelete.setBounds(485, 141, 118, 36);
		frame.getContentPane().add(btnDelete);
	}

	public JFrame getFrame() {
		return frame;
	}

	public void populateJTable(JTable table) {

		String tableColumns[] = { "Domain Name", "Career Name" };
		DefaultTableModel tableModel = new DefaultTableModel(tableColumns, 0);
		
		ArrayList<IDomain> temporaryDomainList = cluster.getDomainsList();
		ArrayList<ICareer> temporaryCareerList;
		
		for (IDomain domain : temporaryDomainList) {
			
			temporaryCareerList = domain.getCareerList();
			
			for(ICareer career : temporaryCareerList) {
				
				ArrayList<String> rowData = new ArrayList<String>();
				rowData.add(domain.getDomainName());
				rowData.add(career.getCareerName());
				tableModel.addRow(rowData.toArray());
			}
			
		}

		table.setModel(tableModel);
	}
}
