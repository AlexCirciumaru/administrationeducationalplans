package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AdminMenu {

	private JFrame frame;
	private ViewEducationPlans viewEducationPlans;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdminMenu window = new AdminMenu();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AdminMenu() {
		initialize();
		viewEducationPlans = new ViewEducationPlans();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 684, 443);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton btnViewEducationPlans = new JButton("View Education Plans");
		btnViewEducationPlans.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				viewEducationPlans.getFrame().setVisible(true);
			}
		});
		btnViewEducationPlans.setBounds(54, 198, 212, 83);
		frame.getContentPane().add(btnViewEducationPlans);
		
		JButton btnStudents = new JButton("Students");
		btnStudents.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new StudentPlans().getFrame().setVisible(true);
			}
		});
		btnStudents.setBounds(353, 198, 234, 83);
		frame.getContentPane().add(btnStudents);
		
		JLabel lblAdminMenu = new JLabel("Admin Menu");
		lblAdminMenu.setFont(new Font("Sitka Text", Font.BOLD | Font.ITALIC, 20));
		lblAdminMenu.setBounds(254, 11, 147, 49);
		frame.getContentPane().add(lblAdminMenu);
	}
}
