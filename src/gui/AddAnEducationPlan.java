package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JTextField;

import controllers.AddAnEducationPlanController;
import model.Cluster;
import model.ICluster;
import model.IDomain;
import serialize.DomainsSerializer;
import verifier.CheckInputs;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;

public class AddAnEducationPlan {

	private JFrame frame;
	private AddAnEducationPlanController controller;
	private JComboBox comboBox;
	private JTextField textFieldDomainName;
	private JTextField textFieldCareerName;
	private JTextField textFieldCareerDescription;
	private JTextField textFieldSubjectName;
	private JTextField textFieldSubjectYear;
	private ICluster cluster;
	private DomainsSerializer serializer;
	private CheckInputs checkInputs;
	private JScrollPane scrollPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					 AddAnEducationPlan window = new AddAnEducationPlan();
					 window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AddAnEducationPlan() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 625, 504);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		controller = new AddAnEducationPlanController();
		serializer = new DomainsSerializer();
		checkInputs = new CheckInputs();

		cluster = serializer.deserialize("domainsList.ser");

		JLabel lblChooseADomain = new JLabel("Choose a Domain :");
		lblChooseADomain.setBounds(37, 41, 110, 27);
		frame.getContentPane().add(lblChooseADomain);

		textFieldDomainName = new JTextField();
		textFieldDomainName.setBounds(399, 55, 152, 27);
		frame.getContentPane().add(textFieldDomainName);
		textFieldDomainName.setColumns(10);

		DefaultComboBoxModel tableModel = new DefaultComboBoxModel(cluster.getDomainsList().toArray());
		
		comboBox = new JComboBox<>();
		comboBox.setModel(tableModel);
		scrollPane = new JScrollPane(comboBox, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setViewportView(comboBox);
		scrollPane.setBounds(157, 43, 158, 39);
		frame.getContentPane().add(scrollPane);

		JLabel lblCareerName = new JLabel("Career Name :");
		lblCareerName.setBounds(37, 127, 95, 27);
		frame.getContentPane().add(lblCareerName);

		textFieldCareerName = new JTextField();
		textFieldCareerName.setBounds(157, 127, 135, 27);
		frame.getContentPane().add(textFieldCareerName);
		textFieldCareerName.setColumns(10);

		textFieldCareerDescription = new JTextField();
		textFieldCareerDescription.setBounds(157, 176, 158, 27);
		frame.getContentPane().add(textFieldCareerDescription);
		textFieldCareerDescription.setColumns(10);

		JLabel lblCareerDescription = new JLabel("Career Description :");
		lblCareerDescription.setBounds(21, 178, 137, 22);
		frame.getContentPane().add(lblCareerDescription);

		JLabel lblSubjectName = new JLabel("Subject Name :");
		lblSubjectName.setBounds(37, 259, 110, 36);
		frame.getContentPane().add(lblSubjectName);

		textFieldSubjectName = new JTextField();
		textFieldSubjectName.setBounds(157, 266, 158, 22);
		frame.getContentPane().add(textFieldSubjectName);
		textFieldSubjectName.setColumns(10);

		JLabel lblSubjectYear = new JLabel("Subject Year :");
		lblSubjectYear.setBounds(37, 306, 95, 27);
		frame.getContentPane().add(lblSubjectYear);

		textFieldSubjectYear = new JTextField();
		textFieldSubjectYear.setBounds(157, 308, 155, 22);
		frame.getContentPane().add(textFieldSubjectYear);
		textFieldSubjectYear.setColumns(10);

		JButton btnAddSubject = new JButton("Add subject");
		btnAddSubject.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (!checkInputs.hasOnlyLetters(textFieldSubjectName.getText())) {

					textFieldSubjectName.setText(null);

				} else if (!checkInputs.checkStudyYears(textFieldSubjectYear.getText())) {

					textFieldSubjectYear.setText(null);

				} else {

					controller.addSubject(textFieldSubjectName.getText(),
							Integer.parseInt(textFieldSubjectYear.getText()));
					textFieldSubjectName.setText(null);
					textFieldSubjectYear.setText(null);

				}
			}
		});
		btnAddSubject.setBounds(395, 275, 134, 36);
		frame.getContentPane().add(btnAddSubject);

		JButton btnCreateEducationPlan = new JButton("Create Education Plan");
		btnCreateEducationPlan.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (textFieldCareerName.getText().equals("") || textFieldCareerDescription.getText().equals("")) {

					JOptionPane.showMessageDialog(null, "Please complete the fields.", "Input Error", 0);

				} else if (controller.checkIfListsAreEmpty()) {

					return;
				} else {

					controller.createEducationPlan((IDomain) comboBox.getSelectedItem(), textFieldCareerName.getText(),
							textFieldCareerDescription.getText());
					checkInputs.clearTextFields(textFieldCareerName, textFieldCareerDescription);
					frame.dispose();
				}

			}
		});
		btnCreateEducationPlan.setBounds(380, 385, 181, 43);
		frame.getContentPane().add(btnCreateEducationPlan);

		JLabel lblAddADomain = new JLabel("Add a Domain :");
		lblAddADomain.setBounds(433, 22, 95, 22);
		frame.getContentPane().add(lblAddADomain);

		JButton btnAddDomain = new JButton("Add Domain");
		btnAddDomain.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				if (textFieldDomainName.getText().equals("")) {
					JOptionPane.showMessageDialog(null, "Please enter a domain.", "InputError", 0);

				}
				else if (!checkInputs.checkIfDomainNameAlreadyExists(textFieldDomainName.getText())) {

					JOptionPane.showMessageDialog(null, "This domain already exists.", "Input Error", 0);
					textFieldDomainName.setText("");

				}
				else {
					controller.addDomain(textFieldDomainName.getText());
					textFieldDomainName.setText(null);
				}
			}

		});
		btnAddDomain.setBounds(419, 94, 120, 27);
		frame.getContentPane().add(btnAddDomain);
	}

	public JFrame getFrame() {
		return frame;
	}
}
