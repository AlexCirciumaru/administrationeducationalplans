package factories;

import model.IStudent;
import model.IStudentPlan;
import model.StudentPlan;

public class StudentPlanFactory {
	
	public IStudentPlan createStudentPlan(IStudent student, String careerName) {
		
		return new StudentPlan(student, careerName);
	}
}
