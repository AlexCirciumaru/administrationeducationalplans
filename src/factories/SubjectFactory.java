package factories;

import model.ISubject;
import model.Subject;

public class SubjectFactory {
	
	public ISubject createSubject(String subjectName, int yearStudy) {
		
		return new Subject(subjectName, yearStudy);
	}
}
