package factories;

import model.IStudent;
import model.Student;

public class StudentFactory {

	public IStudent createStudent(String studentFirstName, String studentLastName) {
		
		return new Student(studentFirstName, studentLastName);
	}
}
