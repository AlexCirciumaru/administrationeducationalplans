package factories;

import java.util.ArrayList;

import model.Domain;
import model.ICareer;
import model.IDomain;

public class DomainFactory {

	public IDomain createDomain(String domainName, ArrayList<ICareer> careersList) {
		
		return new Domain(domainName, careersList);
	}
}
