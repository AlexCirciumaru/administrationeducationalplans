package factories;

import java.util.ArrayList;

import model.Career;
import model.ICareer;
import model.ISubject;

public class CareerFactory {
	
	public ICareer createCareer(String careerName, String careerDescription, ArrayList<ISubject> firstYearSubjects, ArrayList<ISubject> secondYearSubjects, ArrayList<ISubject> thirdYearSubjects) {
		
		return new Career(careerName, careerDescription, firstYearSubjects, secondYearSubjects, thirdYearSubjects);
	}
}
