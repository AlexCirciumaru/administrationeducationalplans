package verifier;

import javax.swing.JOptionPane;
import javax.swing.JTextField;

import model.ICluster;
import model.IDomain;
import serialize.DomainsSerializer;

public class CheckInputs {
	
	private DomainsSerializer serializer = new DomainsSerializer();
	private ICluster cluster = serializer.deserialize("D:\\JavaPrograms\\AdministrationEducationalPlans\\domainsList.ser");
	
	public boolean hasOnlyLetters(String text) {

		if (text.equals("")) {
			
			JOptionPane.showMessageDialog(null, "Please complete the fields.", "Input Error", 0);
			return false;
			
		} else if (!text.matches("[a-zA-Z ]+")) {
			
			JOptionPane.showMessageDialog(null, "Please enter only letters.", "Input Error", 0);
			return false;
			
		}
		
		return true;
	}

	public boolean checkStudyYears(String text) {

		if (text.equals("")) {
			
			JOptionPane.showMessageDialog(null, "Please complete the fields.", "Input Error", 0);
			return false;
			
		} else if (!text.matches("[1-3]")) {
			
			JOptionPane.showMessageDialog(null, "Please enter 1, 2 or 3.", "Input Error", 0);
			return false;
		}
		
		return true;
	}
	
	public boolean checkIfDomainNameAlreadyExists(String text) {
		
		for(IDomain domain : cluster.getDomainsList()) {
			
			if(domain.getDomainName().equals(text)) {
				
				return false;
			}
		}
		
		return true;
	}
	
	public void clearTextFields(JTextField textFieldCareerName, JTextField textFieldCareerDescription) {
		
		textFieldCareerName.setText("");
		textFieldCareerDescription.setText("");
	}
}
