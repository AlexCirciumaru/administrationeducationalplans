package serialize;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import javax.swing.JOptionPane;

import model.IStudentEvidence;
import model.IStudentPlan;

public class StudentPlanSerializer implements Serializable, IStudentPlanSerializer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void serialize(IStudentEvidence studentPlan, String path) {
		// TODO Auto-generated method stub
		try {
			
			FileOutputStream fileOut = new FileOutputStream(path);
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(studentPlan);
			out.close();
			fileOut.close();
			System.out.println("Object is serialized at " + path);
		} catch (IOException e) {
			// TODO: handle exception
			JOptionPane.showMessageDialog(null, "File Error.", "File Error", 0);
		}		
	}

	@Override
	public IStudentEvidence deserialize(String path) {
		// TODO Auto-generated method stub
		IStudentEvidence studentEvidence = null;
		
		try {
			
			FileInputStream fileIn = new FileInputStream(path);
			ObjectInputStream in = new ObjectInputStream(fileIn);
			studentEvidence = (IStudentEvidence) in.readObject();
			in.close();
			fileIn.close();
			
		} catch (IOException | ClassNotFoundException e) {
			// TODO: handle exception
			
			JOptionPane.showMessageDialog(null, "The file doesn't exist.", "File Error", 0);
		}
		
		System.out.println("Deserializing object");
		return studentEvidence;
	}
}
