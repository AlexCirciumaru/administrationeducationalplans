package serialize;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import javax.swing.JOptionPane;

import model.ICluster;

public class DomainsSerializer implements IDomainsSerializer, Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void serialize(ICluster cluster, String path) {
		// TODO Auto-generated method stub
		
		try {
			
			FileOutputStream fileOut = new FileOutputStream(path);
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(cluster);
			out.close();
			fileOut.close();
			System.out.println("Object is serialized at " + path);
		} catch (IOException e) {
			// TODO: handle exception
			JOptionPane.showMessageDialog(null, "File Error.", "File Error", 0);
		}
		
	}

	@Override
	public ICluster deserialize(String path) {
		// TODO Auto-generated method stub
		ICluster cluster = null;
		
		try {
			
			FileInputStream fileIn = new FileInputStream(path);
			ObjectInputStream in = new ObjectInputStream(fileIn);
			cluster = (ICluster) in.readObject();
			in.close();
			fileIn.close();
			
		} catch (IOException | ClassNotFoundException e) {
			// TODO: handle exception
			
			JOptionPane.showMessageDialog(null, "The file doesn't exist.", "File Error", 0);
		}
		
		System.out.println("Deserializing object");
		return cluster;
	}

}
