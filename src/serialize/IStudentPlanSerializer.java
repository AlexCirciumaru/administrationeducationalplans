package serialize;

import model.IStudentEvidence;

public interface IStudentPlanSerializer {
	
	void serialize(IStudentEvidence studentPlan, String path);
	
	IStudentEvidence deserialize(String path);
}
