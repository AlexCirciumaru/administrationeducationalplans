package serialize;

import model.ICluster;

public interface IDomainsSerializer {
	
	void serialize(ICluster cluster, String path);
	
	ICluster deserialize(String path);
}
